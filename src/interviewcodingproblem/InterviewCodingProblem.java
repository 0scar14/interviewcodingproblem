package interviewcodingproblem;

import java.util.Scanner;

/**
 *
 * @author fou.333@gmail.com
 */
public class InterviewCodingProblem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Parametros de Entrada
        //Coordenadas de la esquina derecha superior de la superficie
        System.out.println("INPUT:");
        System.out.println("\nUpper-Right Coordinates");
        System.out.println("-----------------------");
        System.out.print("Coordinate X: ");
        int upperRightCoordinatesX = in.nextInt();
        System.out.print("Coordinate Y: ");
        int upperRightCoordinatesY = in.nextInt();

        //Posicion y orientacion del Mar Rover
        System.out.println("\nMars Rovers");
        System.out.println("-----------");
        System.out.print("Position X: ");
        int positionRoverX = in.nextInt();
        System.out.print("Position Y: ");
        int positionRoverY = in.nextInt();
        System.out.print("Orientation: ");
        String orientationRover = in.next();

        //Letras enviada por la NASA para controlar el Mar Rover
        System.out.println("\nOrder Control NASA");
        System.out.println("------------------");
        System.out.print("Letters: ");
        String sendLetters = in.next();

        //Ejecutar el recorrido enviado
        int i = 0;
        String lettersExecuted = "";
        boolean isLimit = false;     
        
        do {

            char letter = sendLetters.charAt(i);
            lettersExecuted = lettersExecuted + letter;
            i++;
            
            switch (orientationRover) {
                case "N": {
                    switch (letter) {
                        case 'L':
                            orientationRover = "O";
                            break;
                        case 'R':
                            orientationRover = "E";
                            break;
                        case 'M':
                            if (positionRoverY < upperRightCoordinatesY) {
                                positionRoverY++;
                            } else {
                                isLimit = true;
                            }
                            break;
                        default:
                            sendLetters = "";
                            break;
                    }
                }
                break;
                case "S": {
                    switch (letter) {
                        case 'L':
                            orientationRover = "E";
                            break;
                        case 'R':
                            orientationRover = "O";
                            break;
                        case 'M':
                            if (positionRoverY > 0) {
                                positionRoverY--;
                            } else {
                                isLimit = true;
                            }
                            break;
                        default:
                            sendLetters = "";
                            break;
                    }
                }
                break;
                case "O": {
                    switch (letter) {
                        case 'L':
                            orientationRover = "S";
                            break;
                        case 'R':
                            orientationRover = "N";
                            break;
                        case 'M':
                            if (positionRoverX > 0) {
                                positionRoverX--;
                            } else {
                                isLimit = true;
                            }
                            break;
                        default:
                            sendLetters = "";
                            break;
                    }
                }
                break;
                case "E": {
                    switch (letter) {
                        case 'L':
                            orientationRover = "N";
                            break;
                        case 'R':
                            orientationRover = "S";
                            break;
                        case 'M':
                            if (positionRoverX < upperRightCoordinatesX) {
                                positionRoverX++;
                            } else {
                                isLimit = true;
                            }
                            break;
                        default:
                            sendLetters = "=";
                            break;
                    }
                }
                break;
                default:
                    sendLetters = "=";
                    break;
            }
        } while (i != (sendLetters.length()) && !isLimit);

        //Salida/Resultado
        System.out.println("\nOUTPUT:");
        System.out.println(positionRoverX + " " + positionRoverY + " " + orientationRover);
        
        //Excepciones        
        if (isLimit) {
            System.out.println("\nEXCEPTION:");
            System.out.println("The command sends crosses the boundaries of the surface. It was run: "+lettersExecuted);
        }
        
        System.out.println("\nPress 0 and enter to exit.");
        int salir = in.nextInt();
    }
}
